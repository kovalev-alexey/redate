import { defineStore } from 'pinia'

export const useMailingStore = defineStore('mailing', {
  state: () => ({
    price: {
      data: null,
      error: null,
      isLoading: false,
    },
    create: {
      data: null,
      error: null,
      isLoading: false,
    },
    listNew: {
      data: null,
      error: null,
      isLoading: false,
    },
    hide: {
      data: null,
      error: null,
      isLoading: false,
    },
    accept: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
