import { defineStore } from 'pinia'
import { API } from '@/api/apiService.js'
import router from '@/router'

export const useUserStore = defineStore('user', {
  state: () => ({
    userInfo: {
      data: null,
      error: null,
      isLoading: false,
    },
    userBalance: {
      data: null,
      error: {
        status: false,
        message: '',
      },
      isLoading: false,
    },
    userBalanceInRUB: {
      data: {
        rub: null,
        kop: null,
      },
      error: null,
    },
    userPhotoList: {
      data: null,
      error: null,
      isLoading: false,
    },
    userAvatar: {
      data: null,
      error: null,
      isLoading: false,
    },
    registrationStatus: {
      data: null,
      error: {
        status: false,
        message: '',
      },
      isLoading: false,
    },
    premiumStatus: {
      data: null,
      error: {
        status: false,
        message: '',
      },
      isLoading: false,
    },
    tokens: {
      data: JSON.parse(localStorage.getItem('tokens')) || null,
      error: null,
      isLoading: false,
    },
    emailStatus: {
      data: null,
      error: {
        status: false,
        message: '',
      },
      isLoading: false,
    },
    forbidden: {
      data: localStorage.getItem('forbidden') || null,
      isLoading: false,
    },
    email: localStorage.getItem('email') || null,
    complaintsAdd: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),

  actions: {
    async getRegistrationStatus() {
      try {
        this.registrationStatus.isLoading = true

        const registrationStatusResponse = await API.get('/user/status')

        if (!registrationStatusResponse.status) {
          throw new Error(registrationStatusResponse.message)
        }

        this.registrationStatus.data = registrationStatusResponse.data

        this.registrationStatus.isLoading = false

        return this.registrationStatus.data
      } catch (error) {
        this.registrationStatus.error.status = true
        this.registrationStatus.error.message = error.message

        this.registrationStatus.isLoading = false

        return this.registrationStatus.error
      }
    },

    async getPremiumStatus() {
      try {
        this.premiumStatus.isLoading = true

        const premiumStatusResponse = await API.get('/user/premium/status')

        if (!premiumStatusResponse.status) {
          throw new Error(premiumStatusResponse.message)
        }

        this.premiumStatus.data = premiumStatusResponse.data

        this.premiumStatus.isLoading = false

        return this.premiumStatus.data
      } catch (error) {
        this.premiumStatus.error.status = true
        this.premiumStatus.error.message = error.message

        this.premiumStatus.isLoading = false

        return this.premiumStatus.error
      }
    },

    setTokens(data) {
      const date = Date.now()

      const tokens = {
        access: {
          token: data.access.token,
          expiredAt: date + data.access.liveTime,
        },
        refresh: {
          token: data.refresh.token,
          expiredAt: date + data.refresh.liveTime,
        },
      }

      this.tokens.data = tokens

      localStorage.setItem('tokens', JSON.stringify(tokens))
    },

    removeTokens() {
      this.tokens.data = null

      localStorage.removeItem('tokens')
    },

    async updateTokens() {
      try {
        this.tokens.isLoading = true

        const updateTokensRequestData = {
          token: this.tokens.data.refresh.token,
        }

        const updateTokensResponse = await API.post(
          '/auth/token/update',
          updateTokensRequestData
        )

        const { data } = updateTokensResponse

        this.setTokens(data)

        this.tokens.isLoading = false

        return this.tokens.data
      } catch (error) {
        this.tokens.error.status = true
        this.tokens.error.message = error.message

        this.tokens.isLoading = false

        return this.tokens.error
      }
    },

    async getUserInfo() {
      try {
        this.userInfo.isLoading = true

        const userInfoResponse = await API.get('/user/info')

        if (!userInfoResponse.status) {
          throw new Error(userInfoResponse.message)
        }

        this.userInfo.data = userInfoResponse.data

        this.userInfo.isLoading = false

        return this.userInfo.data
      } catch (error) {
        this.userInfo.error = error

        this.userInfo.isLoading = false

        return this.userInfo.error
      }
    },

    async getUserBalance() {
      try {
        this.userBalance.isLoading = true

        const userBalanceRequestData = {
          page: 0,
          count: 500,
        }

        const balanceResponse = await API.post(
          '/balance',
          userBalanceRequestData
        )

        if (!balanceResponse.status) {
          throw new Error(balanceResponse.message)
        }

        this.userBalance.data = balanceResponse.data

        this.userBalance.isLoading = false

        return this.userBalance.data
      } catch (error) {
        this.userBalance.error.status = true
        this.userBalance.error.message = error.message

        this.userBalance.isLoading = false

        return this.userBalance.error
      }
    },

    async getUserBalanceInRUB() {
      try {
        const userBalance = this.userBalance.data
          ? this.userBalance.data
          : await this.getUserBalance()

        this.userBalanceInRUB.data.rub = Math.floor(userBalance.balance / 100)
        this.userBalanceInRUB.data.kop = userBalance.balance % 100

        return this.userBalanceInRUB.data
      } catch (error) {
        this.userBalanceInRUB.error = error

        return this.userBalanceInRUB.error
      }
    },

    async getUserPhotoList() {
      try {
        this.userPhotoList.isLoading = true

        const photoListResponse = await API.get('/user/photo/list')

        if (!photoListResponse.status) {
          throw new Error(photoListResponse.message)
        }

        this.userPhotoList.data = photoListResponse.data

        this.userPhotoList.isLoading = false

        return this.userPhotoList.data
      } catch (error) {
        this.getUserPhotoList().error = error

        return this.userPhotoList.error
      }
    },

    async getUserAvatar() {
      try {
        this.userAvatar.isLoading = true

        const userPhotoList = this.userPhotoList.data
          ? this.userPhotoList.data
          : await this.getUserPhotoList()

        const avatarId = userPhotoList.list.find((el) => el.avatar).id

        const avatar = await API.get(`/user/${avatarId}/avatar/0`)

        this.userAvatar.data = avatar.data

        this.userAvatar.isLoading = false

        return this.userAvatar.data
      } catch (error) {
        this.userAvatar.error = error

        return this.userAvatar.error
      }
    },

    logout() {
      this.removeTokens()

      router.push('/')
    },
  },
})
