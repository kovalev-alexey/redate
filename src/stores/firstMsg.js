import { defineStore } from 'pinia'

export const useFirstMsgStore = defineStore('message', {
  state: () => ({
    msgStatus: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})