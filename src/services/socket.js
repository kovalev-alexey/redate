import SockJS from 'sockjs-client/dist/sockjs.min'
import Stomp from 'stompjs'
import { Api } from '@/api/api'

const WS_URL = import.meta.env.VITE_WS_URL

class Service {
  constructor() {
    this.messagesClient = null
  }

  connect(accessToken, subscriptions, callback) {
    if (this.messagesClient !== null) {
      this.disconnect()
    }

    const socket = new SockJS(WS_URL)

    this.messagesClient = Stomp.over(socket)

    const headers = { Authorization: `Bearer ${accessToken}` }

    if (accessToken !== null) {
      this.messagesClient.connectHeaders = headers
    }

    this.messagesClient.connect(headers, () => {
      console.log('connected')

      this.subscribe(subscriptions, callback, headers)
    })
  }

  disconnect() {
    if (this.messagesClient !== null) {
      this.messagesClient.disconnect()
      this.messagesClient = null
    }
  }

  subscribe(subscriptions, callback, headers) {
    subscriptions.forEach((subscription) => {
      this.messagesClient.subscribe(
        `/${subscription}`,
        (event) => callback(event),
        headers
      )
    })
  }

  async subscriptions() {
    return await Api.socketSubscriptions()
  }
}

export const SocketService = new Service()
