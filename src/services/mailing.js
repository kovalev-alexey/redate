import { Api } from '@/api/api'
import { useMailingStore } from '@/stores/mailing'
import { useDatabaseStore } from '@/stores/database'
import { useStore } from '@/stores/main'
import { useUserStore } from '@/stores/user'
import { SendsService } from '@/views/Sends/services'

class Service {
  constructor() {
    this.mailingStore = useMailingStore()
    this.databaseStore = useDatabaseStore()
    this.store = useStore()
    this.userStore = useUserStore()
  }

  async mailingPrice() {
    this.mailingStore.price.isLoading = true

    const { data, error } = await Api.mailingPrice()

    if (error.status) {
      this.mailingStore.price.error = error

      this.mailingStore.price.isLoading = false

      return
    }

    this.mailingStore.price.data = data

    this.mailingStore.price.isLoading = false
  }

  async create(cityName, datingFormat, message) {
    this.mailingStore.price.create = true

    const citiesList = this.databaseStore.cities.data

    let selectedCity = null

    citiesList.forEach((list) => {
      list.forEach((city) => {
        if (cityName === city.name) {
          selectedCity = city
        }
      })
    })

    if (selectedCity === null) {
      this.mailingStore.create.error = {
        status: true,
        message: 'Не выбран город',
      }

      this.mailingStore.create.isLoading = false

      return
    }

    const datingFormats = [
      { id: 1, title: 'Онлайн общение' },
      { id: 2, title: 'Серьезные отношения' },
      { id: 3, title: 'Спонсорство' },
      { id: 4, title: 'Пойти на свидание' },
      { id: 5, title: 'Путешествия' },
    ]

    let datingFormatId = null

    datingFormats.forEach((datingFormatObj) => {
      if (datingFormatObj.title === datingFormat) {
        datingFormatId = datingFormatObj.id
      }
    })

    if (datingFormatId === null) {
      this.mailingStore.create.error = {
        status: true,
        message: 'Не выбран формат',
      }

      this.mailingStore.create.isLoading = false

      return
    }

    const { data, error } = await Api.createMailing(
      selectedCity.id,
      datingFormatId,
      message
    )

    if (error.status) {
      this.mailingStore.price.error = error

      this.mailingStore.price.isLoading = false

      return
    }

    this.mailingStore.create.data = data

    this.mailingStore.create.isLoading = false

    this.store.newSendWindow = false
    this.store.newSendWindowSuccess = true

    SendsService.fetchMySends()
  }

  async mailingListNew(params) {
    this.mailingStore.listNew.isLoading = true

    // const datingFormats = this.userStore.userInfo.data?.datingFormat || [
    //   1, 2, 3, 4, 5,
    // ]
    const datingFormats = [1, 2, 3, 4, 5]

    const cityId = this.userStore.userInfo.data?.city?.id || ''

    const page = {
      page: 0,
      count: 8,
    }

    const { data, error } = await Api.mailingListNew(
      params?.arr || datingFormats,
      params?.cityId || cityId,
      page,
      params?.minAge || 18,
      params?.maxAge || 65
    )

    if (error.status) {
      this.mailingStore.listNew.error = error

      this.mailingStore.listNew.isLoading = false

      return
    }

    this.mailingStore.listNew.data = data.list

    this.mailingStore.listNew.isLoading = false
  }

  async hideMailingById(id) {
    this.mailingStore.hide.isLoading = true

    const { data, error } = await Api.hideMailingById(id)

    if (error.status) {
      this.mailingStore.hide.error = error

      this.mailingStore.hide.isLoading = false

      return
    }

    this.mailingStore.hide.data = data

    await this.mailingListNew()

    this.mailingStore.hide.isLoading = false
  }

  async mailingAccept(id) {
    this.mailingStore.accept.isLoading = true

    const { data, error } = await Api.mailingAccept(id)

    if (error.status) {
      this.mailingStore.accept.error = error

      this.mailingStore.accept.isLoading = false

      return
    }

    this.mailingStore.accept.data = data

    await this.mailingListNew()

    this.mailingStore.accept.isLoading = false
  }
}

export const MailingService = new Service()
