import { Api } from '@/api/api'
import { useFirstMsgStore } from '@/stores/firstMsg.js'

class Service {
  constructor() {
    this.messageStore = useFirstMsgStore()
  }
  async messageRequest(id, text) {
    const msgStatus = this.messageStore.msgStatus
    msgStatus.isLoading = true

    const { data, error } = await Api.messageRequest(id, text)

    if (error.status) {
      msgStatus.error = error

      msgStatus.isLoading = false

      return
    }

    msgStatus.data = data

    msgStatus.isLoading = false
  }
}
export const FirstMessageService = new Service()
