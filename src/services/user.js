import { useUserStore } from '@/stores/user'
import { Api } from '@/api/api'

class Service {
  async userInfo() {
    const userStore = useUserStore()

    userStore.userInfo.isLoading = true

    const { data, error } = await Api.userInfo()

    if (error.status) {
      userStore.userInfo.error = error

      userStore.userInfo.isLoading = false

      return
    }

    userStore.userInfo.data = data

    userStore.userInfo.isLoading = false
  }

  async complaintsAdd(reason, description, userId) {
    const userStore = useUserStore()

    userStore.complaintsAdd.isLoading = true

    const { data, error } = await Api.complaintsAdd(reason, description, userId)

    if (error.status) {
      userStore.complaintsAdd.error = error

      userStore.complaintsAdd.isLoading = false

      return userStore.complaintsAdd.error
    }

    userStore.complaintsAdd.data = data

    userStore.complaintsAdd.isLoading = false

    return userStore.complaintsAdd.data
  }
}

export const UserService = new Service()
