import { defineStore } from 'pinia'

export const useMyAnswersStore = defineStore('myAnswers', {
  state: () => ({
    myAnswers: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
