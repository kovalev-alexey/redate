import { defineStore } from 'pinia'

export const useSendsStore = defineStore('sends', {
  state: () => ({
    sends: {
      isLoading: false,
      data: null,
      error: null,
    },
  }),
})
