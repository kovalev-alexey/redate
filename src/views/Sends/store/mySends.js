import { defineStore } from 'pinia'

export const useMySendsStore = defineStore('mySends', {
  state: () => ({
    mySend: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
