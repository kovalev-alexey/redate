// import { useDatabaseStore } from '@/stores/database'
import { DatabaseService } from '@/services/database'
import { UserService } from '@/services/user'
import { mySendsApi } from './api'
import { useMySendsStore } from './store/mySends'
import { useMyAnswersStore } from './store/myAnswers'
import { MailingService } from '@/services/mailing'

class Service {
  constructor() {
    this.mySends = useMySendsStore()
    this.myAnswers = useMyAnswersStore()
  }
  async initSendsPage() {
    await this.fetchCities()
    await this.fetchUser()
    MailingService.mailingListNew()
    this.fetchMySends()
  }

  async fetchCities() {
    await DatabaseService.countries()
    await DatabaseService.cities()
  }
  async fetchUser() {
    await UserService.userInfo()
  }

  async fetchMySends() {
    const mySend = this.mySends.mySend
    mySend.isLoading = true

    const sendsStatuses = [1, 2]

    const { data, error } = await mySendsApi.fetchMySends(sendsStatuses)

    if (error.status) {
      mySend.error = error
      mySend.isLoading = false
      return
    }
    // console.log(data.list[0].id)
    this.fetchAnswers(data.list[0].id)
    mySend.data = data.list
    mySend.isLoading = false
    // if (response.error?.status) {
    //   this.mySends.mySends.error = error
    //   this.mySends.isLoading = false
    //   return
    // }
  }

  async fetchOtherSends() {
    console.log('fetching other sends')
  }

  async stopMySend(id) {
    await mySendsApi.stopMySends(id)
    this.fetchMySends()
  }

  async fetchAnswers(id) {
    const myAnswers = this.myAnswers.myAnswers
    myAnswers.isLoading = true
    const { data, error } = await mySendsApi.fetchAnswers(id)
    if (error.status) {
      myAnswers.error = error
      myAnswers.isLoading = false
      return
    }
    myAnswers.data = data.list
    myAnswers.isLoading = false
  }
}

export const SendsService = new Service()
