// import { Api } from '@/api/api.js'
import { API } from '@/api/apiService.js'

class ApiService {
  async fetchMySends(statuses) {
    try {
      const options = {
        page: {
          page: 0,
          count: 30,
        },
        statuses,
      }
      const mySendsResponse = await API.post('/mailing/list/my', options)

      if (!mySendsResponse.status) {
        throw new Error(mySendsResponse.message)
      }
      return {
        data: mySendsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async stopMySends(idMySend) {
    // try {
    const mySendsResponse = await API.get(`/mailing/${idMySend}/stop`)
    console.log('stop my send', mySendsResponse)
    // if (!mySendsResponse.status) {
    //   throw new Error(mySendsResponse.message)
    // }
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Не дописано, нужно сделать, чтобы при успешном удалении запрос на мои сендсы заного обновлялся
    // return {
    //   data: mySendsResponse.data,
    //   error: { status: false, message: '' },
    // }
    // }
    // catch (error) {
    //   return { data: null, error: { status: true, message: error.message } }
    // }
  }
  async fetchAnswers(id) {
    try {
      const options = {
        page: 0,
        count: 30,
      }
      const mySendsResponse = await API.post(`/mailing/${id}/accepts`, options)
      if (!mySendsResponse.status) {
        throw new Error(mySendsResponse.message)
      }
      return {
        data: mySendsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getUserPhoto(idPhoto) {
    try {
      const photoByIdResponse = await API.get(
        `/user/photo/${idPhoto}/0`,
        'arraybuffer'
      )

      if (!photoByIdResponse.status) {
        throw new Error(photoByIdResponse.message)
      }

      return {
        data: `data:image;base64,${btoa(
          String.fromCharCode.apply(
            null,
            new Uint8Array(photoByIdResponse.data)
          )
        )}`,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const mySendsApi = new ApiService()
