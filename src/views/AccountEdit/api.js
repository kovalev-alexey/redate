import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  async changeInfoUser(options) {
    try {
      const response = await API.post('/user/set/info', options)
      console.log(response)
      if (!response.status) {
        throw new Error(response.message)
      }

      return {
        data: response.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  
}

export const AccountEditApi = new ApiService()
