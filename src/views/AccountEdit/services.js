import { UserService } from '@/services/user'
import { DatabaseService } from '@/services/database'
import { AccountEditApi } from './api'
import { useRouter } from 'vue-router'
// import { useUserStore } from '@/stores/user.js'
class Service {
  async init() {
    await this.fetchCities()
    await UserService.userInfo()
  }
  async fetchCities() {
    await DatabaseService.countries()
    await DatabaseService.cities()
  }
  async fetchHobbies() {
    await DatabaseService.hobbies()
  }
  async fetchLangs() {
    await DatabaseService.languages()
  }
  async changeInfo(options) {
    // this.forgotPasswordStore.restore.isLoading = true
    await AccountEditApi.changeInfoUser(options)
    await useRouter().push('/account/:user')
    // if (error.status) {
    //   this.forgotPasswordStore.restore.error = error

    //   this.forgotPasswordStore.restore.isLoading = false

    //   return
    // }

    // localStorage.setItem('email', email)

    // this.userStore.email = email

    // this.forgotPasswordStore.restore.data = data

    // this.forgotPasswordStore.restore.isLoading = false

    // this.router.push({ name: 'Forgot password accept' })
  }
}

export const AccountEditService = new Service()
