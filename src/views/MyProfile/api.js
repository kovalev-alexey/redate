import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  async getPremiumStatus() {
    try {
      const response = await API.get('/user/premium/status')

      if (!response.status) {
        throw new Error(response.message)
      }

      return {
        data: response.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async getBalance() {
    try {
      const balanceRequestData = {
        page: 0,
        count: 30,
      }

      const balanceResponse = await API.post('/balance', balanceRequestData)

      if (!balanceResponse.status) {
        throw new Error(balanceResponse.message)
      }

      return {
        data: balanceResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getRegistrationStatus() {
    try {
      const registrationStatusResponse = await API.get('/user/status')

      if (!registrationStatusResponse.status) {
        throw new Error(registrationStatusResponse.message)
      }

      return {
        data: registrationStatusResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getVerificationStatus() {
    try {
      const verificationStatusResponce = await API.get('/user/verification')

      if (!verificationStatusResponce.status) {
        throw new Error(verificationStatusResponce.message)
      }

      return {
        data: verificationStatusResponce.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getTariffs(sex) {
    try {
      const tarrifsRequestData = {
        sex,
      }

      const tariffsResponce = await API.post('/tariffs', tarrifsRequestData)

      if (!tariffsResponce.status) {
        throw new Error(tariffsResponce.message)
      }

      return {
        data: tariffsResponce.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getPhotoList() {
    try {
      const photoListResponce = await API.get('/user/photo/list')

      if (!photoListResponce.status) {
        throw new Error(photoListResponce.message)
      }

      return {
        data: photoListResponce.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getUserPhoto(idPhoto) {
    try {
      const photoByIdResponse = await API.get(
        `/user/photo/${idPhoto}/0`,
        'arraybuffer'
      )

      if (!photoByIdResponse.status) {
        throw new Error(photoByIdResponse.message)
      }

      return {
        data: `data:image;base64,${btoa(
          String.fromCharCode.apply(
            null,
            new Uint8Array(photoByIdResponse.data)
          )
        )}`,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async addPhoto(file) {
    try {
      const formData = new FormData()

      formData.append('image', file)

      const photoByIdResponse = await API.post('/user/photo/add', formData, {
        'Content-Type': 'multipart/form-data',
      })

      if (!photoByIdResponse.status) {
        throw new Error(photoByIdResponse.message)
      }

      return {
        data: photoByIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async avatarByUserId(id, size = 0) {
    try {
      const avatarByUserIdResponse = await API.get(
        `/user/${id}/avatar/${size}`,
        'arraybuffer'
      )

      if (!avatarByUserIdResponse.status) {
        throw new Error(avatarByUserIdResponse.message)
      }

      return {
        data: avatarByUserIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async setPhotoAvatar(id) {
    try {
      const setAvatarResponse = await API.get(
        `/user/photo/${id}/avatar`
      )

      if (!setAvatarResponse.status) {
        throw new Error(setAvatarResponse.message)
      }

      return {
        data: setAvatarResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async deletePhoto(id) {
    try {
      const deleteResponse = await API.get(
        `/user/photo/${id}/delete`
      )

      if (!deleteResponse.status) {
        throw new Error(deleteResponse.message)
      }

      return {
        data: deleteResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const MyProfileApi = new ApiService()
