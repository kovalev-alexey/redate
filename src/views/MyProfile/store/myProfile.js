import { defineStore } from 'pinia'

export const useMyProfileStore = defineStore('myprofile', {
  state: () => ({
    premiumStatus: {
      data: null,
      error: null,
      isLoading: false,
    },
    balance: {
      data: null,
      error: null,
      isLoading: false,
    },
    registrationStatus: {
      data: null,
      error: null,
      isLoading: false,
    },
    verificationStatus: {
      data: null,
      error: null,
      isLoading: false,
    },
    tariffs: {
      data: null,
      error: null,
      isLoading: false,
    },
    photoList: {
      data: null,
      error: null,
      isLoading: false,
    },
    photoFile: {
      data: null,
      error: null,
      isLoading: false,
    },
    addPhoto: {
      data: null,
      error: null,
      isLoading: false,
    },
    setAvatar: {
      data: null,
      error: null,
      isLoading: false,
    }
  }),
})
