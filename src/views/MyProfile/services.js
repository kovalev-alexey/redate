import { MyProfileApi } from './api'
import { useMyProfileStore } from './store/myProfile'

class Service {
  constructor() {
    this.myProfileStore = useMyProfileStore()
  }
  async getPremiumStatus() {
    this.myProfileStore.premiumStatus.isLoading = true

    const { data, error } = await MyProfileApi.getPremiumStatus()

    if (error.status) {
      this.myProfileStore.premiumStatus.error = error

      this.myProfileStore.premiumStatus.isLoading = false

      return
    }

    this.myProfileStore.premiumStatus.data = data

    this.myProfileStore.premiumStatus.isLoading = false
  }
  async getBalance() {
    this.myProfileStore.balance.isLoading = true

    const { data, error } = await MyProfileApi.getBalance()

    if (error.status) {
      this.myProfileStore.balance.error = error

      this.myProfileStore.balance.isLoading = false

      return
    }

    this.myProfileStore.balance.data = data

    this.myProfileStore.balance.isLoading = false
  }
  async getRegistrationStatus() {
    this.myProfileStore.balance.isLoading = true

    const { data, error } = await MyProfileApi.getRegistrationStatus()

    if (error.status) {
      this.myProfileStore.registrationStatus.error = error

      this.myProfileStore.registrationStatus.isLoading = false

      return
    }

    this.myProfileStore.registrationStatus.data = data

    this.myProfileStore.registrationStatus.isLoading = false
  }
  async getVerificationStatus() {
    this.myProfileStore.verificationStatus.isLoading = true

    const { data, error } = await MyProfileApi.getVerificationStatus()

    if (error.status) {
      this.myProfileStore.verificationStatus.error = error

      this.myProfileStore.verificationStatus.isLoading = false

      return
    }

    this.myProfileStore.verificationStatus.data = data

    this.myProfileStore.verificationStatus.isLoading = false
  }
  async getTariffs(sex) {
    this.myProfileStore.tariffs.isLoading = true

    const { data, error } = await MyProfileApi.getTariffs(sex)

    if (error.status) {
      this.myProfileStore.tariffs.error = error

      this.myProfileStore.tariffs.isLoading = false

      return
    }

    this.myProfileStore.tariffs.data = data

    this.myProfileStore.tariffs.isLoading = false
  }
  async getPhotoList() {
    this.myProfileStore.photoList.isLoading = true

    const { data, error } = await MyProfileApi.getPhotoList()

    if (error.status) {
      this.myProfileStore.photoList.error = error

      this.myProfileStore.photoList.isLoading = false

      return
    }

    this.myProfileStore.photoList.data = data

    this.myProfileStore.photoList.isLoading = false
  }
  async addPhoto(file) {
    this.myProfileStore.addPhoto.isLoading = true

    const { data, error } = await MyProfileApi.addPhoto(file)

    if (error.status) {
      this.myProfileStore.addPhoto.error = error

      this.myProfileStore.addPhoto.isLoading = false

      return
    }

    this.myProfileStore.addPhoto.data = data

    this.myProfileStore.addPhoto.isLoading = false
  }
  async avatarByUserId(id, size) {
    const { data, error } = await MyProfileApi.avatarByUserId(id, size)

    if (error.status) {
      return ''
    }

    return `data:image;base64,${btoa(
      String.fromCharCode.apply(null, new Uint8Array(data))
    )}`
  }
  async setPhotoAvatar(id) {
    this.myProfileStore.setAvatar.isLoading = true

    const { data, error } = await MyProfileApi.setPhotoAvatar(id)

    if (error.status) {
      this.myProfileStore.setAvatar.error = error

      this.myProfileStore.setAvatar.isLoading = false

      return
    }

    this.myProfileStore.setAvatar.data = data

    this.myProfileStore.setAvatar.isLoading = false
  }
}

export const MyProfileService = new Service()
