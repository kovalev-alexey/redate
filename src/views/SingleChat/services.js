import { useSingleChatStore } from './store/singleChat'
import { SingleChatApi } from './api'
import router from '@/router'

class Service {
  async dialog(userId) {
    const singleChatStore = useSingleChatStore()

    singleChatStore.dialog.isLoading = true

    const { data, error } = await SingleChatApi.dialog(userId)

    if (error.status) {
      singleChatStore.dialog.error = error

      singleChatStore.dialog.isLoading = false

      return
    }

    singleChatStore.dialog.data = data

    singleChatStore.dialog.isLoading = false
  }

  async dialogMessages(dialogId, lastId) {
    const singleChatStore = useSingleChatStore()

    singleChatStore.dialogMessages.isLoading = true

    const { data, error } = await SingleChatApi.dialogMessages(dialogId, lastId)

    if (error.status) {
      singleChatStore.dialogMessages.error = error

      singleChatStore.dialogMessages.isLoading = false

      return
    }

    const messages = data.list.reverse()

    messages.push(singleChatStore.dialog.data.lastMessage)

    singleChatStore.dialogMessages.data = messages.reverse()

    singleChatStore.dialogMessages.isLoading = false
  }

  async message(dialogId, companionId, photos, files, message) {
    const singleChatStore = useSingleChatStore()

    singleChatStore.message.isLoading = true

    const { data, error } = await SingleChatApi.message(
      dialogId,
      photos,
      files,
      message
    )

    if (error.status) {
      singleChatStore.message.error = error

      singleChatStore.message.isLoading = false

      return
    }

    singleChatStore.message.data = data

    await this.dialog(companionId)

    await this.dialogMessages(
      dialogId,
      singleChatStore.dialog.data.lastMessage?.id
    )

    singleChatStore.message.isLoading = false
  }

  async readMessages() {
    const singleChatStore = useSingleChatStore()

    singleChatStore.readMessages.isLoading = true

    const messages = singleChatStore.dialogMessages.data

    const unreadedMessages = messages.filter((message) => !message.readed)

    const unreadedMessagesIds = unreadedMessages.map((message) => message.id)

    const promises = []

    unreadedMessagesIds.forEach((messageId) => {
      promises.push(SingleChatApi.readMessage(messageId))
    })

    await Promise.all(promises)

    singleChatStore.readMessages.isLoading = false
  }

  async deleteMessages(messages) {
    const promises = []

    messages.forEach((messageId) =>
      promises.push(SingleChatApi.deleteMessage(messageId))
    )

    await Promise.all(promises)
  }

  async deleteDialog(dialogId) {
    const singleChatStore = useSingleChatStore()

    singleChatStore.deleteDialog.isLoading = true

    const { error } = await SingleChatApi.deleteDialog(dialogId)

    if (error.status) {
      singleChatStore.deleteDialog.error = error

      singleChatStore.deleteDialog.isLoading = false

      return
    }

    singleChatStore.deleteDialog.isLoading = false

    router.push('/chat')
  }
}

export const SingleChatService = new Service()
