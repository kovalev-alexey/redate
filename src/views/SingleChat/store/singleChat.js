import { defineStore } from 'pinia'

export const useSingleChatStore = defineStore('singleChat', {
  state: () => ({
    dialog: {
      data: null,
      error: null,
      isLoading: false,
    },
    dialogMessages: {
      data: null,
      error: null,
      isLoading: false,
    },
    message: {
      data: null,
      error: null,
      isLoading: false,
    },
    readMessages: {
      data: null,
      error: null,
      isLoading: false,
    },
    deleteDialog: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
