import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  async dialog(userid) {
    try {
      const dialogResponse = await API.get(`/dialogs/user/${userid}`)

      if (!dialogResponse.status) {
        throw new Error(dialogResponse.message)
      }

      return {
        data: dialogResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async dialogMessages(dialogId, lastId) {
    try {
      const dialogMessagesRequestData = {
        lastId,
        getAfter: false,
        page: {
          page: 0,
          count: 1000,
        },
      }

      const dialogMessagesResponse = await API.post(
        `/dialogs/${dialogId}/messages`,
        dialogMessagesRequestData
      )

      if (!dialogMessagesResponse.status) {
        throw new Error(dialogMessagesResponse.message)
      }

      return {
        data: dialogMessagesResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async message(dialogId, photos, files, message) {
    try {
      const formData = new FormData()

      formData.append('photos', photos)
      formData.append('photos', files)
      formData.append(
        'message',
        new Blob([
          JSON.stringify({
            message,
          }),
        ])
      )

      const messageResponse = await API.post(
        `/dialogs/${dialogId}/message`,
        formData,
        {
          'Content-Type': 'multipart/form-data',
        }
      )

      if (!messageResponse.status) {
        throw new Error(messageResponse.message)
      }

      return {
        data: messageResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async readMessage(messageId) {
    try {
      const readMessageResponse = await API.get(
        `/dialogs/messages/${messageId}/readed`
      )

      if (!readMessageResponse.status) {
        throw new Error(readMessageResponse.message)
      }

      return {
        data: readMessageResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async deleteMessage(messageId) {
    try {
      const deleteMessageResponse = await API.get(
        `/dialogs/messages/${messageId}/delete`
      )

      if (!deleteMessageResponse.status) {
        throw new Error(deleteMessageResponse.message)
      }

      return {
        data: deleteMessageResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async deleteDialog(dialogId) {
    try {
      const deleteDialogResponse = await API.get(`/dialogs/${dialogId}/delete`)

      if (!deleteDialogResponse.status) {
        throw new Error(deleteDialogResponse.message)
      }

      return {
        data: deleteDialogResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const SingleChatApi = new ApiService()
