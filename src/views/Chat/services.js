import { useChatStore } from './store/chat'
import { ChatApi } from './api'
import { MainService } from '@/views/Main/services'

class Service {
  async dialogs() {
    const chatStore = useChatStore()

    chatStore.dialogs.isLoading = true

    const { data, error } = await ChatApi.dialogs()

    if (error.status) {
      chatStore.dialogs.error = error

      chatStore.dialogs.isLoading = false

      return
    }

    const promises = []

    data.list.forEach((dialog) => {
      promises.push(MainService.avatarByUserId(dialog.user.id))
    })

    const result = await Promise.all(promises)

    data.list.forEach((dialog, idx) => {
      dialog.user.src = result[idx]
    })

    chatStore.dialogs.data = data

    chatStore.dialogs.isLoading = false
  }

  async requests() {
    const chatStore = useChatStore()

    chatStore.requests.isLoading = true

    const { data, error } = await ChatApi.requests()

    if (error.status) {
      chatStore.requests.error = error

      chatStore.requests.isLoading = false

      return
    }

    const promises = []

    data.list.forEach((request) => {
      promises.push(MainService.avatarByUserId(request.user.id))
    })

    const result = await Promise.all(promises)

    data.list.forEach((request, idx) => {
      request.user.src = result[idx]
    })

    chatStore.requests.data = data

    chatStore.requests.isLoading = false
  }

  async dialogPrice(userId) {
    const chatStore = useChatStore()

    chatStore.dialogPrice.isLoading = true

    const { data, error } = await ChatApi.dialogPrice(userId)

    if (error.status) {
      chatStore.dialogPrice.error = error

      chatStore.dialogPrice.isLoading = false

      return
    }

    chatStore.dialogPrice.data = data

    chatStore.dialogPrice.isLoading = false
  }

  async dialogPurchase(userId) {
    const chatStore = useChatStore()

    chatStore.dialogPurchase.isLoading = true

    const { data, error } = await ChatApi.dialogPurchase(userId)

    if (error.status) {
      chatStore.dialogPurchase.error = error

      chatStore.dialogPurchase.isLoading = false

      return chatStore.dialogPurchase.error
    }

    chatStore.dialogPurchase.data = data

    chatStore.dialogPurchase.isLoading = false

    return chatStore.dialogPurchase.data
  }

  async deleteDialogs() {
    const chatStore = useChatStore()

    chatStore.deleteDialogs.isLoading = true

    const { error } = await ChatApi.deleteDialogs()

    if (error.status) {
      chatStore.deleteDialogs.error = error

      chatStore.deleteDialogs.isLoading = false
    }

    chatStore.deleteDialogs.isLoading = false
  }
}

export const ChatService = new Service()
