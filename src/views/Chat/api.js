import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  async dialogs() {
    try {
      const dialogsRequestData = {
        page: 0,
        count: 100,
      }

      const dialogsResponse = await API.post('/dialogs', dialogsRequestData)

      if (!dialogsResponse.status) {
        throw new Error(dialogsResponse.message)
      }

      return {
        data: dialogsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async requests() {
    try {
      const requestsRequestData = {
        page: 0,
        count: 100,
      }

      const requestsResponse = await API.post(
        '/requests/messages',
        requestsRequestData
      )

      if (!requestsResponse.status) {
        throw new Error(requestsResponse.message)
      }

      return {
        data: requestsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async dialogPrice(userId) {
    try {
      const dialogPriceResponse = await API.get(`/user/${userId}/price`)

      if (!dialogPriceResponse.status) {
        throw new Error(dialogPriceResponse.message)
      }

      return {
        data: dialogPriceResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async dialogPurchase(userId) {
    try {
      const dialogPurchaseResponse = await API.get(`/user/${userId}/purchase`)

      if (!dialogPurchaseResponse.status) {
        throw new Error(dialogPurchaseResponse.message)
      }

      return {
        data: dialogPurchaseResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async deleteDialogs() {
    try {
      const deleteDialogsResponse = await API.get('/dialogs/delete')

      if (!deleteDialogsResponse.status) {
        throw new Error(deleteDialogsResponse.message)
      }

      return {
        data: deleteDialogsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const ChatApi = new ApiService()
