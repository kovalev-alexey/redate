import { defineStore } from 'pinia'

export const useChatStore = defineStore('chat', {
  state: () => ({
    dialogs: {
      data: null,
      error: null,
      isLoading: false,
    },
    requests: {
      data: null,
      error: null,
      isLoading: false,
    },
    dialogPrice: {
      data: null,
      error: null,
      isLoading: false,
    },
    dialogPurchase: {
      data: null,
      error: null,
      isLoading: false,
    },
    deleteDialogs: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
