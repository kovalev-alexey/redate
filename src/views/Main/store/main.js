import { defineStore } from 'pinia'

export const useMainStore = defineStore('main', {
  state: () => ({
    usersStatistics: {
      data: null,
      error: null,
      isLoading: false,
    },
    potentialPartners: {
      data: [],
      error: null,
      isLoading: false,
    },
    usersSpecial: {
      data: [],
      error: null,
      isLoading: false,
    },
  }),
})
