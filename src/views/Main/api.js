import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  async usersStatistics() {
    try {
      const usersStatisticsResponse = await API.get('/statistics/users')

      if (!usersStatisticsResponse.status) {
        throw new Error(usersStatisticsResponse.message)
      }

      return {
        data: usersStatisticsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async potentialPartners() {
    try {
      const potentialPartnersRequestData = {
        page: 0,
        count: 30,
      }

      const potentialPartnersResponse = await API.post(
        '/users/potential',
        potentialPartnersRequestData
      )

      if (!potentialPartnersResponse.status) {
        throw new Error(potentialPartnersResponse.message)
      }

      return {
        data: potentialPartnersResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async avatarByUserId(userId) {
    try {
      const avatarByUserIdResponse = await API.get(
        `/user/${userId}/avatar/0`,
        'arraybuffer'
      )

      if (!avatarByUserIdResponse.status) {
        throw new Error(avatarByUserIdResponse.message)
      }

      return {
        data: avatarByUserIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async photoListByUserId(userId) {
    try {
      const photoListByUserIdResponse = await API.get(
        `/user/${userId}/photo/list`
      )

      if (!photoListByUserIdResponse.status) {
        throw new Error(photoListByUserIdResponse.message)
      }

      return {
        data: photoListByUserIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async photoById(photoId) {
    try {
      const photoByIdResponse = await API.get(
        `/user/photo/${photoId}/1`,
        'arraybuffer'
      )

      if (!photoByIdResponse.status) {
        throw new Error(photoByIdResponse.message)
      }

      return {
        data: photoByIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async addFavorites(userId) {
    try {
      const addFavoritesResponse = await API.get(`/favorites/${userId}/add`)

      if (!addFavoritesResponse.status) {
        throw new Error(addFavoritesResponse.message)
      }

      return {
        data: addFavoritesResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async usersSpecial() {
    try {
      const usersSpecialRequestData = {
        page: 0,
        count: 30,
      }

      const usersSpecialResponse = await API.post(
        '/users/special',
        usersSpecialRequestData
      )

      if (!usersSpecialResponse.status) {
        throw new Error(usersSpecialResponse.message)
      }

      return {
        data: usersSpecialResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const MainApi = new ApiService()
