import { useMainStore } from './store/main'
import { MainApi } from './api'

class Service {
  async usersStatistics() {
    const mainStore = useMainStore()

    mainStore.usersStatistics.isLoading = true

    const { data, error } = await MainApi.usersStatistics()

    if (error.status) {
      mainStore.usersStatistics.error = error

      mainStore.usersStatistics.isLoading = false

      return
    }

    mainStore.usersStatistics.data = data

    mainStore.usersStatistics.isLoading = false
  }

  async potentialPartners() {
    const mainStore = useMainStore()

    mainStore.potentialPartners.isLoading = true

    const { data, error } = await MainApi.potentialPartners()

    if (error.status) {
      mainStore.potentialPartners.error = error

      mainStore.potentialPartners.isLoading = false

      return
    }

    mainStore.potentialPartners.data = data

    mainStore.potentialPartners.isLoading = false
  }

  async avatarByUserId(userId) {
    const { data, error } = await MainApi.avatarByUserId(userId)

    if (error.status) {
      return ''
    }

    return `data:image;base64,${btoa(
      String.fromCharCode.apply(null, new Uint8Array(data))
    )}`
  }

  async photoListByUserId(userId) {
    const { data, error } = await MainApi.photoListByUserId(userId)

    if (error.status) {
      return []
    }

    const photos = data.list.filter((photo) => photo.moderated === 1)

    const promises = []

    photos.forEach((photo) => {
      promises.push(MainApi.photoById(photo.id))
    })

    const result = await Promise.all(promises)

    result.forEach((item, idx) => {
      photos[idx] = { ...photos[idx], ...item }
    })

    return photos
      .filter((item) => !item.error.status)
      .map((item) => {
        return {
          ...item,
          src: `data:image;base64,${btoa(
            String.fromCharCode.apply(null, new Uint8Array(item.data))
          )}`,
        }
      })
  }

  async addFavorites(userId) {
    const addFavoritesResponse = MainApi.addFavorites(userId)

    if (addFavoritesResponse.error.status) {
      return
    }

    this.potentialPartners()
  }

  async usersSpecial() {
    const mainStore = useMainStore()

    mainStore.usersSpecial.isLoading = true

    const { data, error } = await MainApi.usersSpecial()

    if (error.status) {
      mainStore.usersSpecial.error = error

      mainStore.usersSpecial.isLoading = false

      return
    }

    mainStore.usersSpecial.data = data

    mainStore.usersSpecial.isLoading = false
  }
}

export const MainService = new Service()
