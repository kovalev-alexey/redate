import { defineStore } from 'pinia'

export const useUserProfileStore = defineStore('userprofile', {
  state: () => ({
    profileInfo: {
      data: null,
      error: null,
      isLoading: false,
    },
    favoriteData: {
      data: null,
      error: null,
      isLoading: false,
    },
    likeData: {
      data: null,
      error: null,
      isLoading: false,
    },
    photoList: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
