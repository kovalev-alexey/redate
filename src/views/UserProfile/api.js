import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }
  async getProfileInfo(userId) {
    try {
      const responseUserInfo = await API.get(`/user/${userId}/info/moderated`)

      if (!responseUserInfo.status) {
        throw new Error(responseUserInfo.message)
      }

      return {
        data: responseUserInfo.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async setFavorite(userId) {
    try {
      const responseFav = await API.get(`/favorites/${userId}/add`)

      if (!responseFav.status) {
        throw new Error(responseFav.message)
      }

      return {
        data: responseFav.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async setLike(userId) {
    try {
      const responseLike = await API.get(`/user/${userId}/like`)

      if (!responseLike.status) {
        throw new Error(responseLike.message)
      }

      return {
        data: responseLike.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getProfilePhotoList(userId) {
    try {
      const responsePhotoList = await API.get(`/user/${userId}/photo/list`)

      if (!responsePhotoList.status) {
        throw new Error(responsePhotoList.message)
      }

      return {
        data: responsePhotoList.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getUserPhoto(idPhoto, size = 1) {
    try {
      const photoByIdResponse = await API.get(
        `/user/photo/${idPhoto}/${size}`,
        'arraybuffer'
      )

      if (!photoByIdResponse.status) {
        throw new Error(photoByIdResponse.message)
      }

      return {
        data: `data:image;base64,${btoa(
          String.fromCharCode.apply(
            null,
            new Uint8Array(photoByIdResponse.data)
          )
        )}`,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async avatarByUserId(id, size = 0) {
    try {
      const avatarByUserIdResponse = await API.get(
        `/user/${id}/avatar/${size}`,
        'arraybuffer'
      )

      if (!avatarByUserIdResponse.status) {
        throw new Error(avatarByUserIdResponse.message)
      }

      return {
        data: avatarByUserIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const UserProfileApi = new ApiService()
