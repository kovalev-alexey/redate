import { UserProfileApi } from './api'
import { useUserProfileStore } from './store/userprofile'

class Service {
  constructor() {
    this.profileStore = useUserProfileStore()
  }
  async getProfileInfo(userId) {
    this.profileStore.profileInfo.isLoading = true

    const { data, error } = await UserProfileApi.getProfileInfo(userId)

    if (error.status) {
      this.profileStore.profileInfo.error = error

      this.profileStore.profileInfo.isLoading = false

      return
    }

    this.profileStore.profileInfo.data = data

    this.profileStore.profileInfo.isLoading = false
  }
  async setFavorite(userId) {
    this.profileStore.favoriteData.isLoading = true

    const { error } = await UserProfileApi.setFavorite(userId)

    if (error.status) {
      this.profileStore.favoriteData.error = error

      this.profileStore.favoriteData.isLoading = false

      return
    }

    this.profileStore.favoriteData.isLoading = false
  }
  async setLike(userId) {
    this.profileStore.likeData.isLoading = true

    const { error } = await UserProfileApi.setLike(userId)

    if (error.status) {
      this.profileStore.likeData.error = error

      this.profileStore.likeData.isLoading = false

      return
    }

    this.profileStore.likeData.isLoading = false
  }
  async getProfilePhotoList(userId) {
    this.profileStore.photoList.isLoading = true

    const { data, error } = await UserProfileApi.getProfilePhotoList(userId)

    if (error.status) {
      this.profileStore.photoList.error = error

      this.profileStore.photoList.isLoading = false

      return
    }

    this.profileStore.photoList.data = data

    this.profileStore.photoList.isLoading = false
  }
  async avatarByUserId(id, size) {
    const { data, error } = await UserProfileApi.avatarByUserId(id, size)

    if (error.status) {
      return ''
    }

    return `data:image;base64,${btoa(
      String.fromCharCode.apply(null, new Uint8Array(data))
    )}`
  }
}

export const ProfileService = new Service()
