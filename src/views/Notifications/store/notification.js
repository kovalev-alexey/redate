import { defineStore } from 'pinia'

export const useNotificationStore = defineStore('notification', {
  state: () => ({
    likeList: {
      data: null,
      error: null,
      isLoading: false,
    },
    viewsList: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
