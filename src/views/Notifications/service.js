import { NotificationApi } from './api'
import { useNotificationStore } from './store/notification.js'

class Service {
  constructor() {
    this.notificationStore = useNotificationStore()
  }
  async getLikeList() {
    this.notificationStore.likeList.isLoading = true

    const { data, error } = await NotificationApi.getLikeList()

    if (error.status) {
      this.notificationStore.likeList.error = error

      this.notificationStore.likeList.isLoading = false

      return
    }

    this.notificationStore.likeList.data = data

    this.notificationStore.likeList.isLoading = false
  }
  async getViewList() {
    this.notificationStore.viewsList.isLoading = true

    const { data, error } = await NotificationApi.getViewList()

    if (error.status) {
      this.notificationStore.viewsList.error = error

      this.notificationStore.viewsList.isLoading = false

      return
    }

    this.notificationStore.viewsList.data = data

    this.notificationStore.viewsList.isLoading = false
  }
  async avatarByUserId(id, size) {
    const { data, error } = await NotificationApi.avatarByUserId(id, size)

    if (error.status) {
      return ''
    }

    return `data:image;base64,${btoa(
      String.fromCharCode.apply(null, new Uint8Array(data))
    )}`
  }
}

export const NotificationService = new Service()
