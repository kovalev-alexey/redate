import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }
  async getLikeList() {
    try {
      const likeListRequest = {
        page: 0,
        count: 30,
      }

      const likeListResponce = await API.post('/user/likes', likeListRequest)

      if (!likeListResponce.status) {
        throw new Error(likeListResponce.message)
      }

      return {
        data: likeListResponce.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getViewList() {
    try {
      const viewsListRequest = {
        page: 0,
        count: 30,
      }

      const viewsListResponce = await API.post('/user/views', viewsListRequest)

      if (!viewsListResponce.status) {
        throw new Error(viewsListResponce.message)
      }

      return {
        data: viewsListResponce.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async avatarByUserId(id, size = 0) {
    try {
      const avatarByUserIdResponse = await API.get(
        `/user/${id}/avatar/${size}`,
        'arraybuffer'
      )

      if (!avatarByUserIdResponse.status) {
        throw new Error(avatarByUserIdResponse.message)
      }

      return {
        data: avatarByUserIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const NotificationApi = new ApiService()
