import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }
  async getFavList() {
    try {
      const favListRequest = {
        page: 0,
        count: 30,
      }

      const favListResponce = await API.post('/favorites', favListRequest)

      if (!favListResponce.status) {
        throw new Error(favListResponce.message)
      }

      return {
        data: favListResponce.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async setRemoveFavList(userId) {
    try {
      const removeFavListResponce = await API.get(`/favorites/${userId}/delete`)
      console.log(removeFavListResponce)
      if (!removeFavListResponce.status) {
        throw new Error(removeFavListResponce.message)
      }

      return {
        data: removeFavListResponce.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getProfilePhotoList(userId) {
    try {
      const responsePhotoList = await API.get(`/user/${userId}/photo/list`)

      if (!responsePhotoList.status) {
        throw new Error(responsePhotoList.message)
      }

      return {
        data: responsePhotoList.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async avatarByUserId(id, size = 0) {
    try {
      const avatarByUserIdResponse = await API.get(
        `/user/${id}/avatar/${size}`,
        'arraybuffer'
      )

      if (!avatarByUserIdResponse.status) {
        throw new Error(avatarByUserIdResponse.message)
      }

      return {
        data: avatarByUserIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async getUserPhoto(idPhoto) {
    try {
      const photoByIdResponse = await API.get(
        `/user/photo/${idPhoto}/0`,
        'arraybuffer'
      )

      if (!photoByIdResponse.status) {
        throw new Error(photoByIdResponse.message)
      }

      return {
        data: `data:image;base64,${btoa(
          String.fromCharCode.apply(
            null,
            new Uint8Array(photoByIdResponse.data)
          )
        )}`,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const FavoritesApi = new ApiService()
