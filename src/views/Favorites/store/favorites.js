import { defineStore } from 'pinia'

export const useFavoritesStore = defineStore('favorites', {
  state: () => ({
    favoritesList: {
      data: null,
      error: null,
      isLoading: false,
    },
    photoList: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
