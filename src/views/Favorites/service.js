import { FavoritesApi } from './api'
import { useFavoritesStore } from './store/favorites.js'

class Service {
  constructor() {
    this.favoritesStore = useFavoritesStore()
  }
  async getFavList() {
    this.favoritesStore.favoritesList.isLoading = true

    const { data, error } = await FavoritesApi.getFavList()

    if (error.status) {
      this.favoritesStore.favoritesList.error = error

      this.favoritesStore.favoritesList.isLoading = false

      return
    }

    this.favoritesStore.favoritesList.data = data

    this.favoritesStore.favoritesList.isLoading = false
  }
  // async setFavList(userId) {
  //   this.favoritesStore.favoritesList.isLoading = true

  //   const { data, error } = await FavoritesApi.setFavList(userId)

  //   if (error.status) {
  //     this.favoritesStore.favoritesList.error = error

  //     this.favoritesStore.favoritesList.isLoading = false

  //     return
  //   }

  //   this.favoritesStore.favoritesList.data = data

  //   this.favoritesStore.favoritesList.isLoading = false
  // }
  async setRemoveFavList(userId) {
    this.favoritesStore.favoritesList.isLoading = true

    const { error } = await FavoritesApi.setRemoveFavList(userId)

    if (error.status) {
      this.favoritesStore.favoritesList.error = error

      this.favoritesStore.favoritesList.isLoading = false

      return
    }

    this.getFavList()

    this.favoritesStore.favoritesList.isLoading = false
  }
  async getProfilePhotoList(userId) {
    this.favoritesStore.photoList.isLoading = true

    const { data, error } = await FavoritesApi.getProfilePhotoList(userId)

    if (error.status) {
      this.favoritesStore.photoList.error = error

      this.favoritesStore.photoList.isLoading = false

      return
    }

    this.favoritesStore.photoList.data = data

    this.favoritesStore.photoList.isLoading = false
  }
  async avatarByUserId(id, size) {
    const { data, error } = await FavoritesApi.avatarByUserId(id, size)

    if (error.status) {
      return ''
    }

    return `data:image;base64,${btoa(
      String.fromCharCode.apply(null, new Uint8Array(data))
    )}`
  }
}

export const FavoritesService = new Service()
