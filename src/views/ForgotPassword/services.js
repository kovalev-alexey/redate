import router from '@/router'
import { useUserStore } from '@/stores/user'
import { useForgotPasswordStore } from './store/forgotPassword'
import { ForgotPasswordApi } from './api'
import { encryptPassword } from '@/services/encrypt'

class Service {
  constructor() {
    this.router = router
    this.userStore = useUserStore()
    this.forgotPasswordStore = useForgotPasswordStore()
  }

  async restore(email) {
    this.forgotPasswordStore.restore.isLoading = true

    const { data, error } = await ForgotPasswordApi.restore(email)

    if (error.status) {
      this.forgotPasswordStore.restore.error = error

      this.forgotPasswordStore.restore.isLoading = false

      return
    }

    localStorage.setItem('email', email)

    this.userStore.email = email

    this.forgotPasswordStore.restore.data = data

    this.forgotPasswordStore.restore.isLoading = false

    this.router.push({ name: 'Forgot password accept' })
  }

  async check(code) {
    this.forgotPasswordStore.check.isLoading = true

    let { email } = this.userStore

    if (!email === null) {
      email = localStorage.getItem('email')
    }

    const codeStr = code.join('')

    const { data, error } = await ForgotPasswordApi.check(email, codeStr)

    if (error.status) {
      this.forgotPasswordStore.check.error = error

      this.forgotPasswordStore.check.isLoading = false

      return
    }

    this.forgotPasswordStore.check.data = data

    this.forgotPasswordStore.check.isLoading = false

    this.router.push({ name: 'Forgot password second' })
  }

  async set(password) {
    this.forgotPasswordStore.set.isLoading = true

    const code = this.forgotPasswordStore.check.data?.code || null

    const encryptedPassword = encryptPassword(password)

    const { data, error } = await ForgotPasswordApi.set(code, encryptedPassword)

    if (error.status) {
      this.forgotPasswordStore.set.error = error

      this.forgotPasswordStore.set.isLoading = false

      return
    }

    this.forgotPasswordStore.set.data = data

    this.forgotPasswordStore.set.isLoading = false

    this.router.push({ name: 'Forgot password success' })
  }
}

export const ForgotPasswordService = new Service()
