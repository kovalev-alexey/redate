import { defineStore } from 'pinia'

export const useForgotPasswordStore = defineStore('forgotPassword', {
  state: () => ({
    restore: {
      data: null,
      error: null,
      isLoading: false,
    },
    check: {
      data: null,
      error: null,
      isLoading: false,
    },
    set: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
