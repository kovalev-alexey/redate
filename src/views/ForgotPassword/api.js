import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  async restore(email) {
    try {
      const restoreRequestData = {
        email,
      }

      const restoreResponse = await API.post(
        '/auth/password/restore',
        restoreRequestData,
        null,
        true
      )

      if (!restoreResponse.status) {
        throw new Error(restoreResponse.message)
      }

      return {
        data: restoreResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async check(email, code) {
    try {
      const checkRequestData = {
        email,
        code,
      }

      const checkResponse = await API.post(
        '/auth/password/check',
        checkRequestData,
        null,
        true
      )

      if (!checkResponse.status) {
        throw new Error(checkResponse.message)
      }

      return {
        data: checkResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async set(code, password) {
    try {
      const setRequestData = {
        code,
        password,
      }

      const setResponse = await API.post(
        '/auth/password/set',
        setRequestData,
        null,
        true
      )

      if (!setResponse.status) {
        throw new Error(setResponse.message)
      }

      return {
        data: setResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const ForgotPasswordApi = new ApiService()
