import { DatabaseService } from '@/services/database'
import { SearchApi } from './api'
import { useSearchStore } from './store/search'
import { useUserStore } from '@/stores/user'

class Service {
  constructor() {
    this.search = useSearchStore()
    this.user = useUserStore()
  }

  async init() {
    await DatabaseService.countries()
    await DatabaseService.cities()
    const filterObj = {
      cityId: this.user.userInfo.data.city.id
    }
    this.fetchUsers(filterObj)
  }

  async fetchUsers(filterObj) {
    const searchData = this.search.search
    searchData.isLoading = true
    const options = {
      datingFormats:filterObj.arr,
      ageMax: filterObj.maxAge,
      ageMin: filterObj.minAge,
      cityId: filterObj.cityId,
      page: {
        page: 0,
        count:30,
      },
      diamond: filterObj.diamond,
      special: filterObj.special,
      online: filterObj.online,
      newUsers: filterObj.newUsers,
    }
    const { data, error } = await SearchApi.fetchUsers(options)
    if (error.status) {
      searchData.error = error
      searchData.isLoading = false
      return
    }
    searchData.data = data

    searchData.isLoading = false
  }

  async photoById(userId, size = 1) {
    const { data, error } = await SearchApi.photoById(userId,size)

    if (error.status) {
      return ''
    }

    return `data:image;base64,${btoa(
      String.fromCharCode.apply(null, new Uint8Array(data))
    )}`
  }

  // Example

  // async deletePhoto(id) {
  //   this.registrationStore.deletePhoto.isLoading = true

  //   const { data, error } = await RegistrationApi.deletePhoto(id)

  //   if (error.status) {
  //     this.registrationStore.deletePhoto.error = error

  //     this.registrationStore.deletePhoto.isLoading = false

  //     return
  //   }

  //   this.registrationStore.deletePhoto.data = data

  //   this.registrationStore.deletePhoto.isLoading = false
  // }
}

export const SearchService = new Service()
