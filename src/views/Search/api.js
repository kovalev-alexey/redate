import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  async fetchUsers(options) {
    console.log(options)
    try {
      const response = await API.post('/users/search', options)
      if (!response.status) {
        throw new Error(response.message)
      }

      return {
        data: response.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
  async photoById(userId,size) {
    try {
      const avatarByUserIdResponse = await API.get(
        `/user/${userId}/avatar/${size}`,
        'arraybuffer'
      )

      if (!avatarByUserIdResponse.status) {
        throw new Error(avatarByUserIdResponse.message)
      }

      return {
        data: avatarByUserIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const SearchApi = new ApiService()
