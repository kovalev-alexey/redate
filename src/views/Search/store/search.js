import { defineStore } from 'pinia'

export const useSearchStore = defineStore('search', {
  state: () => ({
    search: {
      data: null,
      error: null,
      isLoading: false,
    },
  }),
})
