import { defineStore } from 'pinia'

export const useStoreProfile = defineStore('profile', {
  state: () => ({
    blockForFetch: {
      datingFormats: [],
      smoke: 0,
      alcohol: 0,
      country: '62f26eeee33a9b5b73dd37f5', //id
      city: '62f26f31e33a9b5b73dd37f9', //id
      nationality: '62f27354e33a9b5b73dd380e', //id
      bdate: new Date(), //"1996-11-01T21:16:00+0300"
      height: 0,
      weight: 0,
      womenParameters: {
        breast: 0,
        waist: 0,
        tips: 0,
      },
      hobbies: [
        //id
      ],
      languages: [
        //id
      ],
      monthMoney: 100,
      about: 'Lorem Ipsum',
      name: 'User',
    },
    popupFlag: false,
    datingFormats: [],
  }),
  actions: {
    changePopupState() {
      this.popupFlag = !this.popupFlag
    },
    // changeDatingFormats(index) {
    //   this.datingFormats[index].isActive = !this.datingFormats[index].isActive
    // },
    // changeblockForFetch(
    //   hobbies = [],
    //   langs = [],
    //   currentStore
    // ) {
    //   this.blockForFetch.datingFormats = this.datingFormats
    //   // this.blockForFetch.datingFormats = currentStore.value?.datingFormat
    //   this.blockForFetch.smoke = currentStore.value?.smoke
    //   this.blockForFetch.alcohol = currentStore.value?.alcohol
    //   this.blockForFetch.country = currentStore.value?.country.id
    //   this.blockForFetch.city = currentStore.value?.city.id
    //   this.blockForFetch.nationality = currentStore.value?.nationality.id
    //   this.blockForFetch.bdate = new Date()
    //   this.blockForFetch.height = currentStore.value?.height
    //   this.blockForFetch.weight = currentStore.value?.weight
    //   this.blockForFetch.hobbies = hobbies
    //   this.blockForFetch.languages = langs
    //   this.blockForFetch.monthMoney = currentStore.value?.monthMoney
    //   this.blockForFetch.about = currentStore.value?.about
    //   this.blockForFetch.name = currentStore.value?.name
    //   if (currentStore.value?.womenParameters) {
    //     this.blockForFetch.womenParameters.breast =
    //       currentStore.value?.womenParameters.breast
    //     this.blockForFetch.womenParameters.waist =
    //       currentStore.value?.womenParameters.waist
    //     this.blockForFetch.womenParameters.tips =
    //       currentStore.value?.womenParameters.tips
    //   }
    // },
    IdArr(arr) {
      let newIdArr = []
      for (let item in arr) {
        newIdArr.push(arr[item].id)
      }
      return newIdArr
    },
  },
})
