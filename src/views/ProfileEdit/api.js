import { API } from '@/api/apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  ////////////////////////////////////////////////////////////////////////

  async postRefreshData(dataObj) {
    try {
      const languagesResponse = await API.post('/user/set/info', dataObj)

      if (!languagesResponse.status) {
        throw new Error(languagesResponse.message)
      }

      return {
        data: languagesResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}
export const Api = new ApiService()
