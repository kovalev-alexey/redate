import { API } from './apiService.js'

class ApiService {
  constructor() {
    this.API = API
  }

  async countries() {
    try {
      const countriesRequestData = {
        pagination: {
          page: 0,
          count: 500,
        },
      }

      const countriesResponse = await API.post(
        '/database/countries',
        countriesRequestData
      )

      if (!countriesResponse.status) {
        throw new Error(countriesResponse.message)
      }

      return {
        data: countriesResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async cities(countryId) {
    try {
      const citiesRequestData = {
        pagination: {
          page: 0,
          count: 500,
        },
        country: countryId,
      }

      const citiesResponse = await API.post(
        '/database/cities',
        citiesRequestData
      )

      if (!citiesResponse.status) {
        throw new Error(citiesResponse.message)
      }

      return {
        data: citiesResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async languages() {
    try {
      const languagesRequestData = {
        pagination: {
          page: 0,
          count: 500,
        },
      }

      const languagesResponse = await API.post(
        '/database/languages',
        languagesRequestData
      )

      if (!languagesResponse.status) {
        throw new Error(languagesResponse.message)
      }

      return {
        data: languagesResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async nationalities() {
    try {
      const nationalitiesRequestData = {
        pagination: {
          page: 0,
          count: 500,
        },
      }

      const nationalitiesResponse = await API.post(
        '/database/nationalities',
        nationalitiesRequestData
      )

      if (!nationalitiesResponse.status) {
        throw new Error(nationalitiesResponse.message)
      }

      return {
        data: nationalitiesResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async hobbies() {
    try {
      const hobbiesRequestData = {
        pagination: {
          page: 0,
          count: 500,
        },
      }

      const hobbiesResponse = await API.post(
        '/database/hobbies',
        hobbiesRequestData
      )

      if (!hobbiesResponse.status) {
        throw new Error(hobbiesResponse.message)
      }

      return {
        data: hobbiesResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async setUserInfo(info) {
    try {
      const setUserInfoRequestData = info

      const setUserInfoResponse = await API.post(
        '/user/set/info',
        setUserInfoRequestData
      )

      if (!setUserInfoResponse.status) {
        throw new Error(setUserInfoResponse.message)
      }

      return {
        data: setUserInfoResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async userInfo() {
    try {
      const userInfoResponse = await API.get('/user/info')

      if (!userInfoResponse.status) {
        throw new Error(userInfoResponse.message)
      }
      return {
        data: userInfoResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async mailingPrice() {
    try {
      const mailingPriceResponse = await API.get('/mailing/price')

      if (!mailingPriceResponse.status) {
        throw new Error(mailingPriceResponse.message)
      }

      return {
        data: mailingPriceResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async createMailing(cityId, datingFormat, text) {
    try {
      const createMailingRequestData = {
        cityId,
        datingFormat,
        text,
      }

      const mailingCreateResponse = await API.post(
        '/mailing/create',
        createMailingRequestData
      )

      if (!mailingCreateResponse.status) {
        throw new Error(mailingCreateResponse.message)
      }

      return {
        data: mailingCreateResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async mailingListNew(datingFormats, cityId, page, minAge, maxAge) {
    try {
      const mailingListNewRequestData = {
        datingFormats,
        cityId,
        page,
        minAge,
        maxAge,
      }

      const mailingListNewResponse = await API.post(
        '/mailing/list/new',
        mailingListNewRequestData
      )
      console.log(mailingListNewResponse)
      if (!mailingListNewResponse.status) {
        throw new Error(mailingListNewResponse.message)
      }

      return {
        data: mailingListNewResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async hideMailingById(id) {
    try {
      const hideMailingByIdResponse = await API.get(`/mailing/${id}/hide`)

      if (!hideMailingByIdResponse.status) {
        throw new Error(hideMailingByIdResponse.message)
      }

      return {
        data: hideMailingByIdResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async mailingAccept(id) {
    try {
      const mailingAcceptResponse = await API.get(`/mailing/${id}/accept`)

      if (!mailingAcceptResponse.status) {
        throw new Error(mailingAcceptResponse.message)
      }

      return {
        data: mailingAcceptResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async socketSubscriptions() {
    try {
      const socketSubscriptionsResponse = await API.get('/subscriptions')

      if (!socketSubscriptionsResponse.status) {
        throw new Error(socketSubscriptionsResponse.message)
      }

      return {
        data: socketSubscriptionsResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async complaintsAdd(reason, description, userId) {
    try {
      const complaintsAddRequestData = {
        reason,
        description,
        userId,
      }

      const complaintsAddResponse = await API.post(
        '/complaints/add',
        complaintsAddRequestData
      )

      if (!complaintsAddResponse.status) {
        throw new Error(complaintsAddResponse.message)
      }

      return {
        data: complaintsAddResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }

  async messageRequest(id, text) {
    try {
      const messageRequestData = {
        message: text,
      }

      const messageResponse = await API.post(
        `/requests/message/${id}`,
        messageRequestData
      )

      if (!messageResponse.status) {
        throw new Error(messageResponse.message)
      }

      return {
        data: messageResponse.data,
        error: { status: false, message: '' },
      }
    } catch (error) {
      return { data: null, error: { status: true, message: error.message } }
    }
  }
}

export const Api = new ApiService()
