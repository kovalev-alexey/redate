export default (number, delimiter = ' ') =>
  number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter)
